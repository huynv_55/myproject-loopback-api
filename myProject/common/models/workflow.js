module.exports = function(Workflow) {
//function boot load config workflow
  Workflow.loadConfigWorkFlow = function() {

    // load json from load-workflow.json
    var fjson = require('fs');
    var configure = JSON.parse(fjson.readFileSync(__dirname + "/load-workflow.json", "utf8"));

    //destroy All old workflow then create new workflow
    Workflow.destroyAll(function(err, info){
      for (var i = 0; i < configure.length ; i++) {
        Workflow.create(configure[i], function(err, obj){
          if (err) {
            console.log('error when create workflow step');
          } else {
            console.log('success create workflow step');
          }
        }); 
      }
    });
  }

  Workflow.getByStepId = function(stepId, cb) {
    Workflow.find({where : {step_id : stepId} }, function(err, rel){
      var response;
      if (err) {
        response = 'no results';
      } else {
        response = rel[0];
      }
      cb(null, response);
    });
  }

  Workflow.getAllStep = function(cb) {
    Workflow.find({where : { id : {neq : ''} }}, function(err, rel){
      var response;
      if (err) {
        response = 'no results';
      } else {
        response = rel;
      }
      cb(null, response);
    });
  }

  Workflow.remoteMethod(
    'getByStepId',
        {
          http: {path: '/getByStepId', verb: 'get'},
          accepts: {arg: 'step_id', type: 'number', http: { source: 'query' } },
          returns: {arg: 'results', type: 'string'}
        }
  );
  Workflow.remoteMethod(
    'getAllStep',
        {
          http: {path: '/getAllStep', verb: 'get'},
          returns: {arg: 'results', type: 'string'}
        }
  );
};
