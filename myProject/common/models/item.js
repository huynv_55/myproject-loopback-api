module.exports = function(Item) {
	Item.addNewItem = function(new_item, cb) {
		//console.log(new_item);
		if (Object.keys(new_item).indexOf('item_id') >= 0) {
			var itemId = new_item.item_id;
			//console.log(itemId);
			Item.find({where : { item_id : itemId } }, function(err, rel){
				var response;
				if (Object.keys(rel).length === 0) {
					Item.create(new_item, function(err, relsults){
						
						if (err) {
							response = "error";
						} else {
							console.log(relsults);
							response = relsults.id;
						}
						cb(null, response);
					});
				} else {
					Item.updateAll( {item_id : itemId }, new_item, function(err, relsults){
						if (err) {
							response = "error";
						} else {
							Item.find({where : { item_id : itemId }}, function(errfind , report){
								if (err) {
									response = "error";
								} else {
									response = report[0].id;
								}
								cb(null, response);
							});
						}
						
					});
				}
			});
		}
			
	}
	Item.remoteMethod(
		"addNewItem",
		{
			http : { path : "/addNewItem" , verb : "post"},
			accepts: { arg: 'data', type: 'object', http: { source: 'body' } },
			returns: {arg: 'id', type: 'string'}
		}
	);

	Item.changeStep = function(data, cb) {
		//console.log(data);
		var response;
		if (Object.keys(data).indexOf('item_id') >= 0 && Object.keys(data).indexOf('step') >= 0) {
			var itemId = data.item_id;
			var stepId = data.step;
			Item.updateAll( {item_id : itemId }, { step : stepId }, function(err, relsults){
				if (err) {
					response = "error";
				} else {
					response = "success";
				}
				cb(null, response);
						
			});
		} else {
			response = "error";
			cb(null, response);
		}
		
	}
	Item.remoteMethod(
		"changeStep",
		{
			http : { path : "/changeStep" , verb : "post"},
			accepts: { arg: 'data', type: 'object' , http : { source : 'body' } },
			returns: {arg: 'results', type: 'string'}
		}
	);
};
